<!DOCTYPE html>
<html lang="em">
<head>
	<meta charset="UTF-8">
	<meta name ="viewport" content="width=device-width. initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<h1>Tentukan Nilai</h1>

	<?php
	function tentukan_nilai($number)
	{
	 if ($number >= 98){
	    echo "Sangat Baik";
	}elseif($number >=76){
		echo "Baik";
	}elseif($number >= 67){
		echo "Cukup";
	}elseif($number >= 43){
		echo "Kurang";
	}
	}

	//TEST CASES
	echo tentukan_nilai(98); //Sangat Baik
	echo tentukan_nilai(76); //Baik
	echo tentukan_nilai(67); //Cukup
	echo tentukan_nilai(43); //Kurang
	?>

</body>
</html>